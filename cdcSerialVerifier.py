import csv
import glob

with open('/home/lauren/Downloads/CDC Drive Serials.csv') as csv_file:
	csv_reader = csv.reader(csv_file, delimiter=",")
	line_count = 0
	needs_cert = []
	dup_certs = []
	for row in csv_reader:
		if line_count < 2:
			'''ignore'''
		else:
			files = glob.glob('/home/lauren/Downloads/DriveErase/*' + row[0] + '*Success*.pdf')
			if files:
				if len(files) > 1:
					dup_certs.append(row[0])
			else:
				needs_cert.append(row[0])
		line_count += 1
	print(f'The following {str(len(dup_certs))} serials have multiple certificates:') 
	for serial in dup_certs:
		print(serial)
	print(f'The following {str(len(needs_cert))} serials still need certificates:')
	for serial in needs_cert:
		print(serial)